# Secret Hitler Bot for Telegram

This is a project for a Telegram moderator bot for the great game [Secret Hitler](https://www.secrethitler.com/). Kotlin is used as programming language.

This project is currently in the modeling phase, take a look at the [wiki](https://gitlab.com/Eisfunke/secret-hitler-bot-telegram/-/wikis/home).

This is a rewrite from scratch of the [bot we currently use](https://github.com/Eisfunke/Secret-Hitler-Telegram). There is an instance of that running as [`@supersecrethitlerbot`](https://t.me/supersecrethitlerbot). That one is written in Python and has no persistence, so we wanted to start over with a properly typed language.