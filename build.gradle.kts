import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.0"
    kotlin("kapt") version "1.4.0"
    id("org.jetbrains.dokka") version "1.4.0-rc"
    application
}
group = "org.secret-hitler-bot"
version = "0.0.0"

application {
    mainClassName = "secretHitler.MainKt"
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

tasks.dokkaHtml.configure {
    dokkaSourceSets {
        configureEach {
            includes = listOf("packages.md")
        }
    }
}

repositories {
    mavenCentral()
    jcenter()
    maven { url = uri("https://jitpack.io") }
    maven { url = uri("https://dl.bintray.com/arrow-kt/arrow-kt/") }
    maven { url = uri("https://kotlin.bintray.com/kotlinx/") }
}
dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter:5.6.2")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.4.0")
    implementation("org.jetbrains.exposed:exposed-core:0.25.1")
    implementation("org.jetbrains.exposed:exposed-dao:0.25.1")
    implementation("org.jetbrains.exposed:exposed-jdbc:0.25.1")
    implementation("io.github.kotlin-telegram-bot.kotlin-telegram-bot:telegram:5.0.0")
    implementation("io.arrow-kt:arrow-optics:0.10.4")
    implementation("io.arrow-kt:arrow-syntax:0.10.4")
    kapt("io.arrow-kt:arrow-meta:0.10.4")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.1.0")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
