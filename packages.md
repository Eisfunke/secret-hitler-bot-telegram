# Module secret-hitler-bot-telegram

A telegram bot for Secret Hitler.

# Package secretHitler

Main package for Secret Hitler.

# Package secretHitler.action

Contains all the possible player-triggered actions for a game of Secret Hitler. Everything that needs to be persisted
and is triggered by a player is modeled as an action.

# Package secretHitler.bot

Contains the user interface part of the program, that is, the Telegram bot command handlers and so on.

# Package secretHitler.game

Contains the class for the game holding actions.

# Package secretHitler.state

Contains data classes modeling single immutable game states. Only used as vessels to convey a single state, any changes
are done using the actions from [secretHitler.action].