package secretHitler.action

import arrow.core.Either
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import secretHitler.state.State

/**
 * An player-triggered action in the game.
 *
 * Every possible action triggered by a player that needs to be persisted is modeled as a subclass of this abstract
 * class.
 *
 * @property timestamp The time this action was created.
 */
abstract class Action {
    val timestamp: Instant = Clock.System.now()
    abstract fun apply(state: State): Either<String, State>
}