package secretHitler.state

data class Player(
    val name: String,
    val id: Int,
    val role: Role,
    val voted: Boolean,
    val termLimited: Boolean,
    val confirmedNotHitler: Boolean,
    val alive: Boolean
)