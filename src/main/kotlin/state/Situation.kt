package secretHitler.state

enum class Situation {
    LOBBY,
    NOMINATION,
    ELECTION,
    LEG_PRES,
    LEG_CHAN,
    VETO,
    INVESTIGATION,
    SPECIAL_ELECTION,
    EXECUTION,
    GAME_OVER
}