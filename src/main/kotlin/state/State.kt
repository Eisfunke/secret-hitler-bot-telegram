package secretHitler.state

import arrow.core.Option

/**
 * A single immutable state of a game at a given time.
 */
data class State(
    val id: Int,
    val failed: Int,
    val vetoed: Int,
    val enacted: Map<Policy, Int>,
    val situation: Situation,

    val deck: List<Policy>,
    val discarded: List<Policy>,
    val hand: List<Policy>,

    val players: List<Player>,
    val president: Option<Player>,
    val chancellor: Option<Player>,

    val spectators: List<Spectator>,
    val waitingPlayers: List<WaitingPlayer>
)